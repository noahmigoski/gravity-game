extends KinematicBody
class_name Player

# mp
var net_id
var team = "Blue" 

# "physics" 
onready var camera = $Rotation_Helper/Camera
onready var rotation_helper = $Rotation_Helper
onready var bodies = $"/root/Space".bodies
onready var spawns = $"/root/Space".spawn_points
var dir = Vector3()
var vel = Vector3()
var up = Vector3(0,1,0)
var current_g_field = []

# UI
var can_interact: bool = false
signal player_interact

onready var UI = get_node("UI")
onready var PauseMenu = get_node("PauseMenu")
func _on_PauseMenu_player_resume():
	toggle_pausemenu(false)
func _on_PauseMenu_player_shutdown():
	get_tree().quit()
signal player_exit_map
func _on_PauseMenu_player_quit():
	emit_signal("player_exit_map")

# gamemode-specific for UI 
# cp
onready var cur_cp_point_id = null

# stats
var dead = false

var respawn_timer = 0.0
var health_refilling = false 
var health_refill_remaining = 0
var health_refill_rate = 0
var health_refill_timer = 0.0

var strength = 25
var armor = 10.0
var mass = 10.0 + armor/10.0
var max_health = 100 + armor
var health = max_health
var MAX_SPEED = 10 - (mass - 10) * strength/100
var JUMP_SPEED = 8 - (mass - 10) * strength/100
var ACCEL = strength/mass
var DEACCEL= 15.5 + strength/100
export var MOUSE_SENSITIVITY = 0.05
export var CONTROLLER_SENSITIVITY = 2.0

# Weapons
onready var weapons_pack = $Rotation_Helper/WeaponsPack
var curWeaponIndex = 0
onready var flashlight = get_node("Rotation_Helper/Flashlight")


func _ready():
	spawn() # TODO: Server delegated (when applicable)
	bodies.append(self) # for physics processing
	UI.team_label.text = team

func _player_load_data(data: Dictionary):
	weapons_pack.set_weapons_by_array(data.weapon_choices)
	weapons_pack.switch_weapon(curWeaponIndex)
	team = data.team 
	UI.team_label.text = team
	max_health = data.health
	armor = data.armor
	strength = data.strength
	mass = data.mass
	JUMP_SPEED = data.jump
	MAX_SPEED = data.speed

# ------------ health / spawning ----------- #

# Add an automatically clamped amount of health to Player.
func add_health(amount: int):
	if health < max_health:
		health += clamp(amount, 0, max_health - health)
		$Heal_effect.emitting = true
		yield(get_tree().create_timer(2.0), "timeout")
		if !health_refilling:
			$Heal_effect.emitting = false

# Rate is health/second; rate = 5 would give health += 5 each second.
func gradual_add_health(amount: int, rate: int):
	if health < max_health:
		amount = clamp(amount, 0, max_health - health)
		health_refill_remaining = amount
		health_refill_rate = rate
		health_refilling = true
		$Heal_effect.emitting = true

# Stop an in-progress gradual health increase.
func gradual_add_health_interrupt():
	health_refilling = false

# TODO: delegate via server when applicable
func spawn():
	vel = Vector3.ZERO
	weapons_pack.replenish_ammo()
	var rand_index = rand_range(0, len(spawns))
	global_transform.origin = spawns[rand_index]

func teleport(teleport_to: Vector3):
	vel = Vector3.ZERO
	global_transform.origin = teleport_to

# ------------ continuous, death sequence or funnel to other continuous ------ #
func _process(delta):
	# Handle being dead
	if respawn_timer > 0.0:
		respawn_timer -= delta
	elif dead:
		dead = false
		UI.death_banner.visible = false
		health = max_health 
		spawn()

	# death by lack of health
	if health <= 0 and not dead:
		Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
		UI.health_label.text = "You died, sorry"
		UI.death_banner.visible = true
		dead = true
		respawn_timer = 2.0
	# do things while not dead
	elif not dead:
		if !PauseMenu.visible:
			process_input()
			process_UI()
		else:
			process_pause_input()

	process_movement(delta)

# ------------ Continuous MP updates and timers --------------- #
# TODO: bit pack for better networking

func _physics_process(delta):
	if Server.connected:
		send_new_states()

	if health_refilling:
		# Stop healing
		if health_refill_remaining <= 0:
			health_refilling = false
			$Heal_effect.emitting = false
		# Run down healing timer and add 
		else:
			if health_refill_timer >= 0.0:
				health_refill_timer -= delta
			else:
				add_health(health_refill_rate)
				health_refill_remaining -= health_refill_rate
				health_refill_timer = 1.0

# Send server current UDP states.
func send_new_states():
	var gen_states = {
		"time": OS.get_system_time_msecs(), 
		"pos": global_transform.origin, 
		"rot": rotation, 
		"transform": global_transform,
		"weapon_transform": weapons_pack.current_weapon.global_transform,
		"health": health
	}
	Server.send_player_states(gen_states)

# ------------ Inputs --------#

# sub for process_input when paused
func process_pause_input():
	if Input.is_action_just_released("ui_cancel") or Input.is_action_just_released("pad_ui_start"):
		toggle_pausemenu(false)

func process_input():
	# Capture mouse
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	
	# pause menu via escape 
	if Input.is_action_just_released("ui_cancel"):
		toggle_pausemenu(false)
	if Input.is_action_just_released("pad_ui_start"):
		toggle_pausemenu(true)

	# interactions when applicable
	if Input.is_action_just_released("interact") and can_interact:
		emit_signal("player_interact")
	

	# controller looking
	var controller_look_vec = Input.get_vector("pad_look_left", "pad_look_right", "pad_look_down", "pad_look_up")
	rotation_helper.rotate(camera.transform.basis.x, deg2rad(controller_look_vec.y * CONTROLLER_SENSITIVITY * 1))
	self.rotate_object_local(camera.transform.basis.y, deg2rad(controller_look_vec.x * CONTROLLER_SENSITIVITY * -1))

	var camera_rot = rotation_helper.rotation_degrees
	camera_rot.x = clamp(camera_rot.x, -90, 90)
	rotation_helper.rotation_degrees = camera_rot

	# Walking
	dir = Vector3()

	var cam_xform = camera.get_global_transform()
	var input_movement_vector = Vector2()
	
	# the controller way
	input_movement_vector = Input.get_vector("pad_left", "pad_right", "pad_backward", "pad_forward")

	# the easy keyboard way
	if Input.is_action_pressed("movement_forward"):
		input_movement_vector.y += 1
	if Input.is_action_pressed("movement_backward"):
		input_movement_vector.y -= 1
	if Input.is_action_pressed("movement_left"):
		input_movement_vector.x -= 1
	if Input.is_action_pressed("movement_right"):
		input_movement_vector.x += 1
	
	input_movement_vector = input_movement_vector.normalized()
	# Basis vectors are already normalized.
	dir += -cam_xform.basis.z * input_movement_vector.y
	dir += cam_xform.basis.x * input_movement_vector.x

	# Jumping
	if is_on_floor():
		if Input.is_action_just_pressed("movement_jump"):
			vel += JUMP_SPEED * up
	# ----------------------------
	
	# Changing weapons.
	if Input.is_action_just_pressed("pad_switch_weapon"):
		if (curWeaponIndex + 1) > len(weapons_pack.weapons) - 1: # on pad, just cycle back to 0 at end of weapon set
			curWeaponIndex = 0
		else:
			curWeaponIndex = clamp(curWeaponIndex + 1, 0, len(weapons_pack.weapons) - 1)
		weapons_pack.switch_weapon(curWeaponIndex)
		if Server.connected:
			Server.reliable_server_call("Player", "update_cur_weapon", {"new_index": curWeaponIndex})
	if Input.is_action_just_released("switch_weapon_forward"):
		curWeaponIndex = clamp(curWeaponIndex + 1, 0, len(weapons_pack.weapons) - 1)
		weapons_pack.switch_weapon(curWeaponIndex)
		if Server.connected:
			Server.reliable_server_call("Player", "update_cur_weapon", {"new_index": curWeaponIndex})
	if Input.is_action_just_released("switch_weapon_backward"):
		curWeaponIndex = clamp(curWeaponIndex - 1, 0, len(weapons_pack.weapons) - 1)
		weapons_pack.switch_weapon(curWeaponIndex)
		if Server.connected:
			Server.reliable_server_call("Player", "update_cur_weapon", {"new_index": curWeaponIndex})

	# fire weapon
	if Input.is_action_just_pressed("fire"):
		weapons_pack.current_weapon.fire()
	if Input.is_action_just_pressed("alt_fire") and "has_alt_fire" in weapons_pack.current_weapon:
		weapons_pack.current_weapon.alt_fire()

	# flashlight because it annoys me mildly
	if Input.is_action_just_pressed("flashlight_toggle"):
		flashlight.visible = !flashlight.visible


func _input(event):
	# add mouse capture for web
	if Input.is_action_just_pressed("mouse_capture"):
		Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
		
	if Input.is_action_just_pressed("mouse_release"):
		Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)

	# mouse looking		
	if event is InputEventMouseMotion and Input.get_mouse_mode() == Input.MOUSE_MODE_CAPTURED:
		rotation_helper.rotate(camera.transform.basis.x, deg2rad(event.relative.y * MOUSE_SENSITIVITY * -1))
		self.rotate_object_local(camera.transform.basis.y, deg2rad(event.relative.x * MOUSE_SENSITIVITY * -1))

		var camera_rot = rotation_helper.rotation_degrees
		camera_rot.x = clamp(camera_rot.x, -90, 90)
		rotation_helper.rotation_degrees = camera_rot

func toggle_pausemenu(was_from_controller: bool):
	UI.visible = !UI.visible
	PauseMenu.visible = !PauseMenu.visible
	if PauseMenu.visible and not was_from_controller:
		Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
	else:
		Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)

# -----------Gravity, jumping, etc. ------------ #

# Handle automatic orientation with respect to gravity.
func process_movement(delta):
	# reorient up
	global_transform.basis = align_up(global_transform.basis, up)
	
	# project dir into the tangent plane (the one we want to move on)
	dir = dir - dir.dot(up) * up 	# direction the player is trying to move
	dir = dir.normalized()
	
	# project vel onto tangent plane
	var hvel = vel - vel.dot(up) * up # hvel stands for 'horizontal velocity'

	# target is just a dummy variable as far as I can figure.
	var target = dir
	target *= MAX_SPEED
	var accel
	if dir.dot(hvel) > 0:
		accel = ACCEL
	else:
		accel = DEACCEL
	hvel = hvel.linear_interpolate(target, accel * delta)
	
	# only allowed to walk on floor
	if is_on_floor():
		vel = hvel + vel.dot(up) * up
	
	# actually move the player
	vel = move_and_slide(vel, up)

# movement (gravity) helper
func align_up(node_basis, normal):
	# this came from [here](https://old.reddit.com/r/godot/comments/a8q93e/orient_node_to_normal_of_ground/)
	var result = Basis()
	result.x = normal.cross(node_basis.z)
	result.y = normal
	result.z = node_basis.x.cross(normal)
	result = result.orthonormalized()
	return result

# ----------- any UI updates ------------ #

func update_score_UI():
	for t in ModeManager.scores.keys():
		var new_text = "%s: %d" % [t, ModeManager.scores[t]]
		$UI.scores.get_node(t).text = new_text

func process_UI():
	if Input.is_action_just_pressed("ui_cancel"):
		if Input.get_mouse_mode() == Input.MOUSE_MODE_VISIBLE:
			Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
		else:
			Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
	
	# personal/under InfoCorner
	if len(weapons_pack.weapons) == 0:
		return
	health = round(health)
	UI.health_label.text = "Health: " + str(health)
	UI.weapon_label.text = weapons_pack.current_weapon.weapon_name
	var curAmmoShow = weapons_pack.current_weapon.ammo
	if (curAmmoShow is float):
		curAmmoShow = round(curAmmoShow)
	UI.ammo_label.text = str(curAmmoShow) + " / " + str(weapons_pack.current_weapon.max_ammo)
	UI.weapon_icon.texture = weapons_pack.current_weapon.icon_image
	UI.ammo_icon.texture = weapons_pack.current_weapon.ammo_icon

	# Scores and progress info
	if UI.progress_on:
		# Capture Point #
		if ModeManager.current_mode == ModeManager.game_modes.CP and cur_cp_point_id != null:
			if Server.connected:
				if !(team in ModeManager.cp_timers[cur_cp_point_id]):
					return
				var local_point = get_node(Server.net_object_local_paths[cur_cp_point_id])
				if local_point.captured_by != team:
					UI.progress_bar.value = ((ModeManager.CP_CAPTURE_TIME - ModeManager.cp_timers[cur_cp_point_id][team]) / ModeManager.CP_CAPTURE_TIME) * 100

# Register with an interactable and display a prompt if available,
# otherwise unregister so we can connect to other interactables.
func handle_interactable(prompt: String, interactor):
	# we entered an interactable area
	if prompt != "":
		UI.interact.text = prompt
		connect("player_interact", interactor, "enable_interaction")
		can_interact = true
	# we left an interactable area
	else:
		UI.interact.text = ""
		disconnect("player_interact", interactor, "enable_interaction")
		can_interact = false


# out of bounds death
func _on_RespawnBox_body_entered(body):
	if body.name == "Player":
		Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
		UI.health_label.text = "Out of Bounds"
		UI.death_banner.visible = true
		dead = true
		respawn_timer = 2.0
