extends Weapon

const has_alt_fire = true

func _ready():
	weapon_name = "Air Gun"
	max_ammo = 100
	ammo = max_ammo
	fire_mode = modes.CONTINUOUS
	projectile_scene = preload("res://local/player/weapons/Air_Gun/Air.tscn")

	alt_projectile_scene = preload("res://local/player/weapons/Air_Gun/Air_Blast.tscn")

	self_refills = true
	refill_rate = .05
	is_continuous_firing = false
	fire_point = get_node("FirePoint")
	icon_image = preload("res://assets/models/Sprites/Silhouette/flamethrower.png")
	ammo_icon = preload("res://assets/images/PNG (Transparent)/smoke_06.png")

func _process(_delta):
	$Air/Air_effect.emitting = is_continuous_firing
