extends Projectile
const blowback_speed = 20
const duration = 2
var duration_timer = duration
var separation = 0

# Called when the node enters the scene tree for the first time.
func _ready():
	mass = 10
	init_vel = 5
	decrement = 20

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
#	process_movement(delta)
	if duration_timer > 0:
		duration_timer -= delta
	else:
		queue_free()


func _on_Area_body_entered(body):
	var away = body.transform.origin - transform.origin
	if body is KinematicBody:
		var separation = sqrt(away.dot(away))
		if separation >= 1:
			body.vel += away.normalized() * blowback_speed / separation
		elif separation < 1 and separation > 0:
			body.vel += away.normalized() * blowback_speed
	elif body is RigidBody:
		body.apply_central_impulse(50 * away.normalized() * blowback_speed / separation)
