extends Weapon
# Called when the node enters the scene tree for the first time.
func _ready():
	type = "melee"
	weapon_name = "Sword"
	fire_mode = modes.MELEE
	icon_image = preload("res://assets/models/Sprites/Silhouette/knife_sharp.png")
