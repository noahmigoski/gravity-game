extends RigidBody
class_name Projectile

var init_vel
var up = Vector3(0,1,0)
var damage
var mu = .01
var decrement # ammo cost per shot, usually 1
var stuck = false
var will_self_destruct = true
var lifespan = 4.0
var shooter = null

onready var bodies = $"/root/Space".bodies
onready var planets = $"/root/Space".planets
var current_g_field = []

func _ready():
	contacts_reported = 1
	contact_monitor = true
	gravity_scale = 0
	friction = mu # this is depricated I guess

func stick_to(collider):
	stuck = true
	sleeping = true
	bodies.erase(self)
	$CollisionShape.disabled = true
	var old_transform = global_transform
	get_parent().remove_child(self)
	collider.add_child(self)
	set_global_transform(old_transform)

func _physics_process(delta):
	if will_self_destruct:
		if lifespan > 0.0:
			lifespan -= delta
		else:
			bodies.erase(self)
			queue_free()
