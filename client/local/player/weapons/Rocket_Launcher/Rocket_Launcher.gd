extends Weapon

# Called when the node enters the scene tree for the first time.
func _ready():
	weapon_name = "RPG, Babieeeee"
	max_ammo = 50
	ammo = max_ammo
	fire_mode = modes.SEMI
	fire_cap = .8
	projectile_scene = preload("res://local/player/weapons/Rocket_Launcher/Rocket.tscn")

	fire_point = get_node("FirePoint")
	has_animation = true
	icon_image = preload("res://assets/models/Sprites/Silhouette/rocketlauncher.png")
	ammo_icon = preload("res://assets/models/Sprites/Silhouette/ammo_rocket.png")
