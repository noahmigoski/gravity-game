extends Explosive

const plot_scene = preload("res://scripted_assets/world/game_mode_interactors/Plot.tscn")

func _ready():
	# assign projectile values
	init_vel = 50
	mass = 1.0
	damage = 33.0
	decrement = 1
	bodies.append(self)


func _on_Rocket_body_shape_entered(_body_id, _body, _body_shape, _local_shape):
	if _body is StaticBody:
		print("PLANT!")
		var plot_clone = plot_scene.instance()
		plot_clone.global_transform = global_transform
		plot_clone.look_at_from_position(global_transform.origin, up, Vector3(0,1,0))
		get_tree().root.add_child(plot_clone)
	explode()
