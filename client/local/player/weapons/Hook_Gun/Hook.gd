extends Projectile

var collider

func _ready():
	mass = 0.5
	init_vel = 25
	bodies.append(self)
	decrement = 1
	will_self_destruct = false

func _process(_delta):
	pass
#	if stuck:
#		transform.origin = collider.transform.origin + sep_from_collider

func _on_Hook_body_shape_entered(_body_id, body, _body_shape, _local_shape):
	if body is Explosive:
		body.explode()
		bodies.erase(self)
		queue_free()
	else:
		collider = body
		stick_to(body)
