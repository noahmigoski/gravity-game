extends Weapon
var chain_length = 15
onready var bodies = $"/root/Space".bodies
const has_alt_fire = true
var tension

var hook_sep

func _ready():
	weapon_name = "Grappling Hook"
	max_ammo = 5
	ammo = max_ammo
	fire_mode = modes.SEMI
	projectile_scene = preload("res://local/player/weapons/Hook_Gun/Hook.tscn")

	fire_point = get_node("FirePoint")

func free_hook():
	if projectile_clone != null:
		projectile_clone.queue_free()
		bodies.erase(projectile_clone)
		$Chain.visible = false
		projectile_clone = null
	

func _process(_delta):
	if projectile_clone != null:
		var hook_point = projectile_clone.global_transform.origin
		var hook_dif = hook_point - fire_point.global_transform.origin
		hook_sep = hook_dif.length()
		var hook_dir = (hook_dif).normalized()
		# Update chain position and length
		$Chain.visible = true
		$Chain.global_transform.origin = (projectile_clone.global_transform.origin + player.global_transform.origin)/2
		$Chain.transform.basis.z = $Chain.transform.basis.z.normalized() * hook_sep/2
		$Chain.look_at(hook_point, Vector3(0,1,0))
		
		# Deal with tension force from chain on both objects
		if projectile_clone.stuck:
			if player.vel.dot(hook_dir) <= 0 and hook_sep >= chain_length:
				# Player connected to player
				if projectile_clone.collider is KinematicBody:
					tension = player.vel.dot(hook_dir) * hook_dir - projectile_clone.collider.vel.dot(hook_dir) * hook_dir
					player.vel = player.vel - tension * (projectile_clone.collider.mass/player.mass)
					projectile_clone.collider.vel = projectile_clone.collider.vel + tension * (player.mass/projectile_clone.collider.mass)
				# Player connected to ball
				elif projectile_clone.collider is RigidBody:
					tension = player.vel.dot(hook_dir) * hook_dir - projectile_clone.collider.linear_velocity.dot(hook_dir) * hook_dir
					#Player.vel = Player.vel - tension * (projectile_clone.collider.mass/Player.mass)
					#projectile_clone.collider.apply_impulse(tension*60, hook_point)
					projectile_clone.collider.apply_central_impulse(tension*60)
				# Player connected to environment
				else:
					player.vel = player.vel - player.vel.dot(hook_dir) * hook_dir
		elif hook_sep > chain_length:
			free_hook()
