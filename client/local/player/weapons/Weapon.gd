extends Spatial
class_name Weapon

# A generic weapon class.
#
# Should handle all weapon's attributes: name, ammo, fire modes
# Children can override fire functions, but this generic class
# should be able to facilitate basic fire modes with some simple args.

var weapon_name
var is_current_weapon = false

var ammo
var max_ammo
var fire_mode
var alt_fire_mode
enum modes {SEMI, AUTO, CONTINUOUS, BURST, MELEE}
var type # gun, melee

var fire_rate = .5 # for auto
var fire_cap = .2 # for semi-auto
var burst_rate = 2# for burst
var burst_amount
var left_in_burst
var burst_in_progress = false
var auto_timer = 0
var burst_timer = 0
var cap_timer = 0
var self_refills # bool: true if this weapon refills ammo at some rate when not in use
var refill_rate # float: ignore if self_refills = false
var is_continuous_firing = false
var can_fire = true
var has_animation = false

var projectile_clone
var projectile_scene # scene for ammo types
var alt_projectile_scene # scene for ammo type of alt_fire
var icon_image
var ammo_icon

onready var player = get_parent().get_parent().get_parent()
# firepoint is set in _process() to keep it up to date as the player moves
var fire_point
# Called when the node enters the scene tree for the first time.
func _ready():
	# add myself to player's inventory
	var pack = get_parent()
	pack.weapons.append(self)

# main fire 
func fire():
	if (!is_current_weapon):
		return

	if fire_mode == modes.SEMI:
		if player.weapons_pack.current_weapon.weapon_name == "Grappling Hook":
			player.weapons_pack.current_weapon.free_hook()
		semi_fire(projectile_scene)
	elif fire_mode == modes.CONTINUOUS:
		is_continuous_firing = true
	elif fire_mode == modes.AUTO:
		try_auto_fire()	
	elif fire_mode == modes.BURST:
		try_burst_fire()
	elif fire_mode == modes.MELEE:
		melee()
	else:
		semi_fire(projectile_scene)
 
# exactly same as fire, but called from alt Input in Player?
func alt_fire():
	if (!is_current_weapon):
		return
	
	if alt_fire_mode == modes.SEMI:
		semi_fire(alt_projectile_scene)
	elif alt_fire_mode == modes.CONTINUOUS:
		is_continuous_firing = true	
	elif alt_fire_mode == modes.AUTO:
		try_auto_fire()	
	elif alt_fire_mode == modes.BURST:
		try_burst_fire()
	elif player.weapons_pack.current_weapon.weapon_name == "Grappling Hook":
		player.weapons_pack.current_weapon.free_hook()
	elif player.weapons_pack.current_weapon.weapon_name == "Dart Gun":
		player.weapons_pack.current_weapon.set_sticky = !player.weapons_pack.current_weapon.set_sticky
	else:
		semi_fire(alt_projectile_scene)
	pass

# TODO func change_fire_mode

# FIX potential error in how this gets called by online players
func semi_fire(projectile):
	if has_animation:
		$AnimationPlayer.play("Recoil")
	if ammo > 0:
		if Server.connected and player is Player:
			Server.reliable_server_call("Firing", "fire_projectile")
		projectile_clone = projectile.instance()
		get_tree().root.add_child(projectile_clone)
		ammo -= projectile_clone.decrement
		projectile_clone.global_transform = fire_point.global_transform
		projectile_clone.set_linear_velocity(player.vel + fire_point.global_transform.basis.z * projectile_clone.init_vel * -1)
		if player is Player: 
			player.vel += float(projectile_clone.mass / player.mass) * projectile_clone.init_vel * fire_point.global_transform.basis.z

# Absolutely cannot make a circular server or local call
func network_semi_fire():
	projectile_clone = projectile_scene.instance()
	if fire_mode == modes.CONTINUOUS:
		is_continuous_firing = true
	if has_animation:
		$AnimationPlayer.play("Recoil")
	if ammo > 0:
		get_tree().root.add_child(projectile_clone)
		ammo -= projectile_clone.decrement
		projectile_clone.global_transform = fire_point.global_transform
		projectile_clone.set_linear_velocity(player.vel + fire_point.global_transform.basis.z * projectile_clone.init_vel * -1)

func try_auto_fire():
	if (can_fire and auto_timer <= 0):
		auto_timer = fire_rate
		can_fire = false
		is_continuous_firing = true
		semi_fire(projectile_scene)

# only does the first shot to get it going, the rest is tracked in _process
func try_burst_fire():
	if (can_fire and burst_timer <= 0):
		burst_in_progress = true
		auto_timer = fire_rate
		burst_timer = burst_rate
		left_in_burst = burst_amount - 1
		semi_fire(projectile_scene)

# timer-based firing or refilling
func _process(delta):
	if (!is_current_weapon):
		return
	if !(player is Player):
		return
	
	# refills
	if self_refills && can_fire && ammo < max_ammo:
		add_ammo(refill_rate)

	# continuous firing 
	if can_fire && Input.is_action_pressed("fire") and fire_mode == modes.CONTINUOUS:
		semi_fire(projectile_scene)
	if Input.is_action_just_released("fire") and fire_mode == modes.CONTINUOUS:
		is_continuous_firing = false
		Server.reliable_server_call("Firing", "stop_continuous_fire")
	# auto
	if Input.is_action_pressed("fire") and fire_mode == modes.AUTO:
		try_auto_fire()
	# rest of a burst or ending it
	if burst_in_progress and left_in_burst > 0 and auto_timer < 0:
		semi_fire(projectile_scene)
		auto_timer = fire_rate
		left_in_burst -= 1
	if (burst_in_progress and left_in_burst <= 0):
		burst_in_progress = false
	
	# run down timers
	if (auto_timer > 0 and (fire_mode == modes.AUTO or fire_mode == modes.BURST)):
		auto_timer -= delta
	if (cap_timer > 0 and fire_mode == modes.SEMI):
		cap_timer -= delta
	if (burst_timer > 0 and fire_mode == modes.BURST):
		burst_timer -= delta
	if (!can_fire and ammo > 0 and (auto_timer <= 0 or cap_timer <= 0 or burst_timer <= 0)):
		can_fire = true

func melee():
	$AnimationPlayer.play("Swoosh")
	var melee_damage = 50
	for body in $Area.get_overlapping_bodies():
		if body is KinematicBody && body != Player:
				body.vel += -10 * player.global_transform.basis.z
		if "health" in body && body != player:
			body.health -= melee_damage

func add_ammo(amount):
	if type != "melee":
		ammo += amount
		ammo = clamp(ammo, 0, max_ammo)
