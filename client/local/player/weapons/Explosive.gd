extends Projectile
class_name Explosive

var BLAST_SPEED = 15
var EXPLOSION_WAIT_TIME = .48
var explosion_wait_timer = 0
var seperation = 0
onready var mesh = $CollisionShape/mesh
onready var blast_area = $Blast_Area
var away
var explosion_particles
var explosion

func _ready():
	explosion_particles = preload("res://local/player/weapons/Explosion.tscn")

func explode():
	explosion = explosion_particles.instance()
	get_tree().root.add_child(explosion)
	explosion.global_transform = global_transform
	mesh.visible = false
	var effected_bodies = blast_area.get_overlapping_bodies()
	for body in effected_bodies:
		away = body.transform.origin - transform.origin
		seperation = away.length()
		if body is KinematicBody:
			if seperation >= 1:
				body.vel += away.normalized() * BLAST_SPEED / seperation
			elif seperation < 1 and seperation > 0:
				body.vel += away.normalized() * BLAST_SPEED
		elif body is RigidBody:
			if seperation >= 1:
				body.apply_central_impulse(away.normalized() * BLAST_SPEED / seperation)
			elif seperation < 1 and seperation > 0:
				body.apply_central_impulse(away.normalized() * BLAST_SPEED)
		if "health" in body:
			if seperation >= 1:
				body.health -= damage / seperation
			elif seperation < 1 and seperation > 0:
				body.health -= damage
	bodies.erase(self)
	#yield(get_tree().create_timer(0.5), "timeout") 
	# this might cause problems but I don't want to pass delta to explode
	queue_free()
