extends Spatial


var explosion_timer = 0
const EXPLOSION_TIME = 2

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func _process(delta):
	if explosion_timer < EXPLOSION_TIME:
		explosion_timer += delta
	else:
		queue_free()
