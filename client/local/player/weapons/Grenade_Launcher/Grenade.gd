extends Explosive


const GRENADE_TIME = 2
var grenade_timer = 0

func _ready():
	init_vel = 15
	mass = 2.0
	damage = 33.0
	mu = .05
	decrement = 1
	bodies.append(self)

func _process(delta):
	if grenade_timer < GRENADE_TIME:
		grenade_timer += delta
	else:
		explode()
