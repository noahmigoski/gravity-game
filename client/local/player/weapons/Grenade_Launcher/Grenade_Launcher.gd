extends Weapon

# Called when the node enters the scene tree for the first time.
func _ready():
	weapon_name = "Thumper"
	max_ammo = 100
	ammo = max_ammo
	fire_mode = modes.BURST
	burst_amount = 3
	left_in_burst = burst_amount
	burst_rate = 2
	fire_rate = .3
	projectile_scene = preload("res://local/player/weapons/Grenade_Launcher/Grenade.tscn")

	fire_point = get_node("FirePoint")
	has_animation = true
	icon_image = preload("res://assets/models/Sprites/Silhouette/rocketlauncherModern.png")
	ammo_icon = preload("res://assets/models/Sprites/Silhouette/grenade.png")
