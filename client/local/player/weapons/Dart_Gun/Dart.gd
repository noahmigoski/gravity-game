extends Projectile

var sticky = true
const DART_TIME = 10
var dart_timer = 0

func _ready():
	mass = 0.1
	init_vel = 25
	mu = 1
	damage = 5
	bodies.append(self)
	decrement = 1

func _process(delta):
	if dart_timer < DART_TIME:
		dart_timer += delta
	else:
		bodies.erase(self)
		queue_free()

func _on_Dart_body_shape_entered(_body_id, body, _body_shape, _local_shape):
	if body is Explosive:
		body.explode()
		bodies.erase(self)
		queue_free()
	elif "health" in body:
		body.health -= damage
		bodies.erase(self)
		queue_free()
	else:
		if sticky:
			stick_to(body)
