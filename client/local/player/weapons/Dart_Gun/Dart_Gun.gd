extends Weapon

var set_sticky = true
const has_alt_fire = true
onready var bodies = $"/root/Space".bodies
# Called when the node enters the scene tree for the first time.
func _ready():
	weapon_name = "Dart Gun"
	max_ammo = 200
	ammo = max_ammo
	fire_mode = modes.AUTO
	fire_rate = .1
	projectile_scene = preload("res://local/player/weapons/Dart_Gun/Dart.tscn")

	fire_point = get_node("FirePoint")
	has_animation = true
	icon_image = preload("res://assets/models/Sprites/Silhouette/pistol.png")
	ammo_icon = preload("res://assets/images/PNG (Transparent)/circle_05.png")

func _process(_delta):
	if projectile_clone in bodies:
		projectile_clone.sticky = set_sticky
