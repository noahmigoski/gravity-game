extends Spatial

var current_weapon_index: int = 0
var current_weapon: Weapon
var weapons = []

func _ready():
	weapons = get_children() # assumes we have all weapons unless corrected
	current_weapon = weapons[current_weapon_index]
	switch_weapon(current_weapon_index)

func switch_weapon(weapon_index: int):
	# TODO: switching animations would be triggered here? (network and local)
	# deactivate previous
	current_weapon.visible = false
	current_weapon.is_current_weapon = false
	# switch to new
	current_weapon = weapons[weapon_index]
	current_weapon.is_current_weapon = true
	current_weapon.visible = true

# add ammo for all weapons in this Player's inventory
func replenish_ammo():
	for w in weapons:
		w.add_ammo(w.max_ammo) # will be clamped to max no matter what; can pass a value later

func get_weapon_path(weapon_name: String):
	return "res://local/player/weapons/" + weapon_name + "/" + weapon_name + ".tscn"

# FIXME: 
# Eventually weapons should be somehow attached to 'hands', and therefore 
# nicely appear in the viewport no matter what, but we do not currently have hands, 
# so when we instance weapons to replace the full set on map loads, we need to fix
# their local translations/rotations.
export var weapon_offsets = {
	"Grenade_Launcher": {
		"translation": Vector3(1.351, 1.236, -.428),
		"rotation": Vector3(8.735, 7.545, 0)
	},
	"Rocket_Launcher": {
		"translation": Vector3(.805, 1.414, 0),
		"rotation": Vector3(1.95, 1.386, .72),
		#"scale": Vector3(.5, .5, .5)
	},
	"Dart_Gun": {
		"translation": Vector3(.786, 1.43, -1.661),
		"rotation": Vector3(4.07, 4.83, 0)
	},
	"Air_Gun": {
		"translation": Vector3(.812, 1.466, -.628),
		"rotation": Vector3(0,0,0)
	},
	"Hook_Gun": {
		"translation": Vector3(.047, .685, -1.8),
		"rotation": Vector3(0,0,0)
	},
	"Sword": {
		"translation": Vector3(0, .827, -1.399),
		"rotation": Vector3(66.127, -180, 0)
	},

}

func set_weapons_by_array(weapons_name_list: Array):
	# reset
	weapons = []
	for w in get_children():
		remove_child(w)
		w.queue_free()
	# refill
	for w in weapons_name_list:
		var loaded = load(get_weapon_path(w))
		var i = loaded.instance()
		add_child(i)
		# fix some position of weapons
		i.translate(weapon_offsets[w].translation)
		i.rotation_degrees = weapon_offsets[w].rotation	
		if weapon_offsets[w].has("scale"):
			i.scale = weapon_offsets[w].scale
		i.visible = false
	weapons = get_children()
	current_weapon = weapons[0]
	switch_weapon(0)
