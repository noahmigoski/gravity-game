extends MarginContainer

func _ready():
	# Start out not visible, will be toggled later
	visible = false

signal player_resume				# signal to tell Player to resume
func _on_Resume_pressed():	# signal from button to then tell Player
	emit_signal("player_resume")

signal player_quit
func _on_Quit_pressed():
	emit_signal("player_quit")

signal player_shutdown
func _on_Quit2_pressed():
	emit_signal("player_shutdown")

func start_focus():
	$VBoxContainer/Resume.grab_focus()
