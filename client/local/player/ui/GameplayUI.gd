extends Control

onready var crosshair = $crosshair
onready var interact = $interact

onready var health_label = $InfoCorner/V/health
onready var team_label = $InfoCorner/V/team
onready var weapon_label = $InfoCorner/V/Weapon/LeftCol/weapon_label
onready var ammo_label = $InfoCorner/V/Weapon/LeftCol/ammo_label
onready var weapon_icon = $InfoCorner/V/Weapon/RightCol/weapon_icon
onready var ammo_icon = $InfoCorner/V/Weapon/RightCol/ammo_icon

onready var death_banner = $Death

onready var scores = $scores/H 

onready var progress_on = false
onready var progress_bar = $progress/V/bar
onready var progress_status = $progress/V/progress_status

func _ready():
	pass
