extends MarginContainer

# player attributes to be chosen
var armor = 0
var strength = 50
var health = 100
var mass = 10.0
var speed = 10
var jump = 8
var player_choices = {}
var weapons = []
var teams = ["Red", "Green", "Blue"]

onready var swordBtn = get_node("VBoxContainer/HBoxContainer/VBoxContainer2/Weapon1/Control/SwordBtn")
onready var hookBtn = get_node("VBoxContainer/HBoxContainer/VBoxContainer2/Weapon2/Control/HookBtn")
onready var airBtn = get_node("VBoxContainer/HBoxContainer/VBoxContainer2/Weapon3/Control/AirGunBtn")
onready var dartBtn = get_node("VBoxContainer/HBoxContainer/VBoxContainer2/Weapon4/Control/DartGunBtn")
onready var grenadeBtn = get_node("VBoxContainer/HBoxContainer/VBoxContainer2/Weapon5/Control/GrenadeBtn")
onready var rocketBtn = get_node("VBoxContainer/HBoxContainer/VBoxContainer2/Weapon6/Control/RocketBtn")
onready var option_button = get_node("VBoxContainer/HBoxContainer/VBoxContainer/Team_Select/Control/OptionButton")


func _ready():
	for team in teams:
		option_button.add_item(team)


# launching maps, passing player choices
func _on_Go_pressed():
	save_player_choices()
	SceneManager.goto_map("FlatTesting", player_choices)
func _on_Go2_pressed():
	save_player_choices()
	SceneManager.goto_map("OriginalSpace", player_choices)
func _on_Go3_pressed():
	save_player_choices()
	SceneManager.goto_map("Farm", player_choices)

# save choices besides armor-related ones
func save_player_choices():
	player_choices["weapon_choices"] = weapons
	var selected_team = teams[$VBoxContainer/HBoxContainer/VBoxContainer/Team_Select/Control/OptionButton.selected]
	player_choices["team"] = selected_team
	player_choices["armor"] = armor
	player_choices["strength"] = strength 
	player_choices["health"] = health 
	player_choices["mass"] = mass 
	player_choices["speed"] = speed 
	player_choices["jump"] = jump

#---------- Input Interactions ---------------#

func _on_port_input_text_changed(new_text:String):
	Server.port = int(new_text)

func _on_ip_input_text_changed(new_text:String):
	Server.ip = new_text

func _on_armor_value_changed(value):
	armor = value
	strength = float($VBoxContainer/HBoxContainer/VBoxContainer/Option4/Control/Label.text)
	health = 100 + value
	mass = 10 + armor/10.0
	speed = 10 - (mass - 10) * strength/100
	jump = 8 - (mass - 10) * strength/100
	$VBoxContainer/HBoxContainer/VBoxContainer/Option2/Control/Label.text = String(health)
	$VBoxContainer/HBoxContainer/VBoxContainer/Option3/Control/Label.text = String(mass)
	$VBoxContainer/HBoxContainer/VBoxContainer/Option5/Control/Label.text = String(speed)
	$VBoxContainer/HBoxContainer/VBoxContainer/Option6/Control/Label.text = String(jump)
	player_choices["armor"] = armor
	player_choices["strength"] = strength 
	player_choices["health"] = health 
	player_choices["mass"] = mass 
	player_choices["speed"] = speed 
	player_choices["jump"] = jump

func CheckDisableBtns():
	if not swordBtn.pressed:
		swordBtn.disabled = (len(weapons) >= 3)
	if not hookBtn.pressed:
		hookBtn.disabled = (len(weapons) >= 3)
	if not airBtn.pressed:
		airBtn.disabled = len(weapons) >= 3
	if not dartBtn.pressed:
		dartBtn.disabled = len(weapons) >= 3
	if not grenadeBtn.pressed:
		grenadeBtn.disabled = len(weapons) >= 3
	if not rocketBtn.pressed:
		rocketBtn.disabled = len(weapons) >= 3	

func _on_AirGunBtn_toggled(button_pressed):
	if button_pressed:
		weapons.append("Air_Gun")
	elif not button_pressed:
		weapons.erase("Air_Gun")
	CheckDisableBtns()

func _on_DartGunBtn_toggled(button_pressed):
	if button_pressed:
		weapons.append("Dart_Gun")
	elif not button_pressed:
		weapons.erase("Dart_Gun")
	CheckDisableBtns()

func _on_GrenadeBtn_toggled(button_pressed):
	if button_pressed:
		weapons.append("Grenade_Launcher")
	elif not button_pressed:
		weapons.erase("Grenade_Launcher")
	CheckDisableBtns()

func _on_RocketBtn_toggled(button_pressed):
	if button_pressed:
		weapons.append("Rocket_Launcher")
	elif not button_pressed:
		weapons.erase("Rocket_Launcher")
	CheckDisableBtns()

func _on_SwordBtn_toggled(button_pressed):
	if button_pressed:
		weapons.append("Sword")
	elif not button_pressed:
		weapons.erase("Sword")
	CheckDisableBtns()

func _on_HookBtn_toggled(button_pressed):
	if button_pressed:
		weapons.append("Hook_Gun")
	elif not button_pressed:
		weapons.erase("Hook_Gun")
	CheckDisableBtns()

