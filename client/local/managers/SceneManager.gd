extends Node

# TODO: Scene transitions/basic loading animation proof of concept

var current_scene = null
var loaded_by_manager: bool = false
var cur_preplay_data: Dictionary = {}
var cur_map_data: Dictionary = {}


func _ready():
	var root = get_tree().get_root()
	current_scene = root.get_child(root.get_child_count() - 1)

func goto_mainmenu():
	get_tree().change_scene("res://scripted_assets/maps/MenuMap.tscn")


# Change scene to given map with little fuss, 
# but saving the necessary passed data until map 
# is loaded and requests it.
func goto_map(map_name: String, passed_data: Dictionary):
	loaded_by_manager = true
	cur_map_data = map_data[map_name]
	cur_preplay_data = passed_data
	get_tree().change_scene("res://scripted_assets/maps/" + map_name + ".tscn")

# Do things that need to happen right after the scene is loaded
# but before gameplay.
func finish_level_setup():
	current_scene.load_map_data(cur_map_data)
	current_scene.load_preplay_data(cur_preplay_data)
	Server.connect_to_server()

var map_data = {
	"FlatTesting": {
		"flat_gravity": true 
	},
	"OriginalSpace": {
		"flat_gravity": false 
	},
	"Farm": {
		"flat_gravity": false 
	}

}