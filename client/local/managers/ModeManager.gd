extends Node

# By "manager" I more mean "translator":
# Keeps track of game mode-related info and allow players to update UIs. 
# Also make necessary calls/translations from game Server regarding state of a game

# ----------- any mode ------------ #

var active_teams = ["Red", "Blue", "Green"]
var team_info = {
	"Red": {
		"color": "#ff0000",
		"players": []
	},
	"Green": {
		"color": "#00ff00",
		"players": []
	},
	"Blue": {
		"color": "#0000ff",
		"players": []
	},
} # team_name -> {color, players, ...}

var scores
var GAME_TIME_LIMIT 
var game_timer 
var current_mode
enum game_modes {
	NO_GAMEPLAY,
	CP,
	TDM,
	SND,
	GOLF,
}

func _ready():
	current_mode = game_modes.NO_GAMEPLAY
	scores = {}

# ----------- GAME MODE RESET / SETUPS ------ #

func catchup(data):
	current_mode = data.mode
	scores = data.scores

	# specific to modes
	# Capture Point #
	if current_mode == game_modes.CP:
		var points_grabbed = $"/root/Space/ControlPoints".get_children()
		for p in points_grabbed:
			p.visible = true
			var net_object = p.get_node("NetObject")
			net_object.states = data.cp_states[net_object.net_id]
			if net_object.states["is_captured"]:
				p.indicate_capture(net_object.states["captured_by"])

# Capture Point #

const CP_CAPTURE_TIME = 2.0
var cp_points = {} # id -> {object, is_captured, captured_by, num_players_on, ...} 
var cp_timers = {}

func cp_player_entered_point(point_id):
	if Server.connected:
		Server.reliable_server_call("ModeManager", "cp_player_entered_point", {"point_id": point_id})
		Server.local_player.cur_cp_point_id = point_id

func cp_player_exited_point(point_id):
	if Server.connected:
		Server.reliable_server_call("ModeManager", "cp_player_exited_point", {"point_id": point_id})
		Server.local_player.cur_cp_point_id = null

# OTHER MODE #

# ---------- run down timers --------- #

func _physics_process(_delta):
	pass

func _process(_delta):
	pass
