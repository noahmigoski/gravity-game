# pip install gdown pydrive'
from pydrive.auth import GoogleAuth
from pydrive.drive import GoogleDrive
import gdown
import zipfile
import shutil
import os       
import sys
import subprocess

subprocess.check_call([sys.executable, '-m', 'pip', 'install','-q', 'pydrive'])
subprocess.check_call([sys.executable, '-m', 'pip', 'install','-q', 'gdown'])
subprocess.check_call([sys.executable, '-m', 'pip', 'install','-q', 'httplib2==0.15.0'])

remote_id = ""

# Try to download the assets folder, if successful 
# automatically unzip and clean up old file when done.
#
# Would like this to be possible even without credentials 
# so you can still programmatically download without the 
# ability to also upload. (i.e. save valid id somewhere and
# allow publically available since assets are public anyway)
def download():
  # https://stackoverflow.com/questions/24419188/automating-pydrive-verification-process
  gauth = GoogleAuth()
  gauth.LoadCredentialsFile(".mycreds.txt")
  if gauth.credentials is None:
    gauth.LocalWebserverAuth()
  elif gauth.access_token_expired:
      gauth.Refresh()
  else:
      gauth.Authorize()
  gauth.SaveCredentialsFile(".mycreds.txt")
  global_drive = GoogleDrive(gauth)

  # using auth'd drive to find a valid id for the asset folder
  file_list = global_drive.ListFile({'q': "'root' in parents and trashed=false"}).GetList()
  for f in file_list:
    if f['title'].split(".")[0] == "assets":
      print("[SCRIPT] Detected remote assets file, id: " + f['id'])
      remote_id = f['id']

  try:
    url = 'https://drive.google.com/uc?id=' + remote_id
    output = 'assets.zip'
    gdown.download(url, output, quiet=False)
  except:
    print("[SCRIPT] Download failed, didn't find file.")
    exit()

  # extract and cleanup
  print("[SCRIPT] Unzipping..")
  with zipfile.ZipFile("assets.zip", 'r') as zip_ref:
    zip_ref.extractall("assets")
  print("[SCRIPT] Removing old .zip..")
  os.remove("assets.zip")
  print("[SCRIPT] assets loaded.")

# If you have auth, then you can upload assets back up 
def upload():
  # https://stackoverflow.com/questions/24419188/automating-pydrive-verification-process
  gauth = GoogleAuth()
  gauth.LoadCredentialsFile(".mycreds.txt")
  if gauth.credentials is None:
    gauth.LocalWebserverAuth()
  elif gauth.access_token_expired:
      gauth.Refresh()
  else:
      gauth.Authorize()
  gauth.SaveCredentialsFile(".mycreds.txt")
  global_drive = GoogleDrive(gauth)

  # find old assets and delete before uploading new
  remote_id = ""
  file_list = global_drive.ListFile({'q': "'root' in parents and trashed=false"}).GetList()
  for f in file_list:
    if f['title'].split(".")[0] == "assets":
      print("[SCRIPT] Detected remote assets file, id: " + f['id'])
      remote_id = f['id']
  if remote_id != "":
    print("[SCRIPT] Clearing old assets folder..")
    todelete = global_drive.CreateFile({'id': remote_id})
    todelete.Trash()  # Move file to trash.
    todelete.UnTrash()  # Move file out of trash.
    todelete.Delete()  # Permanently delete the file.

  # zip and upload assets folder
  print("[SCRIPT] Zipping current assets folder..")
  shutil.make_archive('assets', 'zip', 'assets')
  print("[SCRIPT] Creating and uploading..")
  zipped = global_drive.CreateFile({'title': 'assets.zip'})
  zipped.SetContentFile('assets.zip')
  zipped.Upload()
  print("[SCRIPT] Setting permissions..")
  zipped.InsertPermission({
    'type': 'anyone',
    'value': 'anyone',
    'role': 'reader'
  })
  # Upload done, clean up
  print("[SCRIPT] Removing local zip..")
  os.remove("assets.zip")
  print("[SCRIPT] Upload complete.")

# input process
if len(sys.argv) < 2:
  print("Usage: python3 load-assets.py [upload|download]\n")
  exit()
elif sys.argv[1] == 'upload':
  upload()
elif sys.argv[1] == 'download':
  download()
else:
  print("Usage: python3 load-assets.py [upload|download]\n")
