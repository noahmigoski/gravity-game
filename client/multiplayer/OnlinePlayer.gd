extends KinematicBody
class_name OnlinePlayer

var net_id
var display_name

# Physics and core game logic
var camera
var rotation_helper
var dir = Vector3()
var vel = Vector3()
var up = Vector3(0,1,0)
var current_g_field = []
onready var bodies = $"/root/Space".bodies
onready var spawns = $"/root/Space".spawn_points
# -----------------

# Stats
var strength = 50 #running force
var armor = 10.0
var mass = 10.0 + armor/10.0
var max_health = 100 + armor
var health = max_health
var MAX_SPEED = 10 - (mass - 10) * strength/100
var JUMP_SPEED = 8 - (mass - 10) * strength/100
var ACCEL = strength/mass
var DEACCEL= 15.5 + strength/100
var MOUSE_SENSITIVITY = 0.05
var Team = {"name":"Team 1", "color":Color(0,255,0)}
# ---------------------------

# Weapons
onready var weapons_pack = $Rotation_Helper/WeaponsPack
var current_weapon_index = 0

func _ready():
	rotation_helper = $Rotation_Helper

	spawn()
	bodies.append(self) # for physics processing

func add_health(amount):
	if health < max_health:
		health += amount
		if health > max_health:
			health = max_health

func spawn():
	var rand_index = rand_range(0, len(spawns))
	global_transform.origin = spawns[rand_index]

func switch_weapon(remote_weapon_index):
	weapons_pack.switch_weapon(remote_weapon_index)

