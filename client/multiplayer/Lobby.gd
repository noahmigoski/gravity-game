extends Spatial
const G = 8
var planets = []
var bodies = []
var spawn_points = []
var path_speeds = {}
# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	for body in bodies:
		var g = gravity(body)
		body.up = -g.normalized()
		if body is KinematicBody:
			body.vel += delta * g/body.mass
		if body is RigidBody:
			body.add_central_force(g)
	move_along_paths(delta)

func move_along_paths(delta):
	for path in path_speeds:
		path.offset += delta*path_speeds[path]
	 # keep this as a var incase we want to have realistic orbits (Keplar's laws)
	# if you increases the speed, decrease the bake interval.
	# Also, always set rotation mode to none in PathFollow
#	$Path/PathFollow.offset += delta * speed * 10
#	$Path2/PathFollow.offset += delta * speed
	

func gravity(body):
	var r
	var end_gravity = Vector3()
	for field in body.current_g_field:
		r = body.global_transform.origin - field.global_transform.origin
		end_gravity += - G * field.mass * body.mass / r.dot(r) * r.normalized()
	return end_gravity
