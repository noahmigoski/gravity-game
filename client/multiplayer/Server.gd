# CLIENT server code

extends Node

var network = NetworkedMultiplayerENet.new()
var ip = "127.0.0.1"
var port = 4567
var connected = false

onready var other_player_temp = load("res://multiplayer/OnlinePlayer.tscn")
var local_player
var players_container 
var net_object_local_paths = {}

signal net_objects_initialize

func _ready():
	pass

# ------------- basic peer connection --------- #

func connect_to_server():
	print("[Server] Attempting connect to server..")
	network.create_client(ip, port)
	get_tree().set_network_peer(network)

	# connecting signals related to hopping on the server
	network.connect("connection_failed", self, "_OnConnectionFailed")
	network.connect("connection_succeeded", self, "_OnConnectionSucceeded")

func _OnConnectionFailed():
	print("[Server] Failed to connect.")
	print("[Server] Spawning just on local instance.")


func _OnConnectionSucceeded():
	# prove connection
	print("[Server] Successfully connected")
	reliable_server_call("Test", "get_test_server_message")
	connected = true
	local_player = $"../Space/Player"
	players_container = $"../Space/Players"
	# send player-specific load settings to server to be reflected
	# FIXME: team not always decided by player
	reliable_server_call("Player", "load_weapon_choices", {"weapon_choices": SceneManager.cur_preplay_data.weapon_choices, "team": local_player.team, "health": local_player.health, "max_health": local_player.max_health})
	emit_signal("net_objects_initialize")
	reliable_server_call("Player", "catchup_data")

# ---------- Registering net objects --------- #

remote func set_my_net_id(player_id):
	local_player.net_id = str(player_id)
	reliable_server_call("ModeManager", "gamemode_catchup")

remote func add_connected_player(player_id):
	if !(str(player_id) == local_player.net_id):
		var new_player = other_player_temp.instance()
		new_player.name = str(player_id)
		new_player.net_id = str(player_id)
		players_container.add_child(new_player)
		
remote func remove_disconnected_player(player_id):
	if !(str(player_id) == local_player.net_id):
		var to_remove = get_node("../Space/Players/" + str(player_id))
		get_node("../Space").bodies.erase(to_remove)
		to_remove.queue_free()

# Recieve info about registered net object
remote func register_net_object(net_id, local_path, catchup_data = {}):
	var local_object = get_node(str(local_path) + "/NetObject")
	local_object.net_id = net_id
	local_object.states = catchup_data
	if "cur_transform" in catchup_data:
		local_object.get_parent().global_transform = catchup_data["cur_transform"]
		local_object.authority_player_net_id = catchup_data["authority"]
	net_object_local_paths[net_id] = local_path

# -------------- UDP updates -------------- #

func send_player_states(new_states):
	rpc_unreliable_id(1, "update_states", new_states)

remote func update_local_transforms(states):
	var players = players_container.get_children()
	for p in players:
		if states.has(p.net_id):
			p.global_transform = states[p.net_id]["transform"]
			p.weapons_pack.current_weapon.global_transform = states[p.net_id]["weapon_transform"]

remote func net_object_loc_update(object_net_id, data):
	if !(object_net_id in Server.net_object_local_paths):
		return
	var to_place_path = Server.net_object_local_paths[object_net_id]
	var to_place = get_node(to_place_path)
	if str(to_place.get_node("NetObject").authority_player_net_id) != local_player.net_id:
		to_place.global_transform = data["transform"]

# -------------- TCP updates -------------- #

func reliable_server_call(funnel_category: String, function: String, data = {}):
	rpc_id(1, "reliable_server_call", funnel_category, function, data)

# ----------- TCP reciever functions --------- #

remote func return_test_message(message):
	print("The secret message is: " + message)

remote func recieve_catchup_data(data):
	var players = players_container.get_children()
	for p in players:
		get_node("../Space/Players/" + str(p.net_id)).weapons_pack.set_weapons_by_array(data[str(p.net_id)].weapon_choices)

remote func gamemode_catchup(data):
	ModeManager.catchup(data)

# ----------- firing #
remote func update_fire_projectile(shooter):
	if !(str(shooter) == local_player.net_id):
		get_node("../Space/Players/" + str(shooter)).weapons_pack.current_weapon.network_semi_fire()

remote func stop_continuous_fire(shooter):
	if !(str(shooter) == local_player.net_id):
		get_node("../Space/Players/" + str(shooter)).weapons_pack.current_weapon.is_continuous_firing = false

# ---------- weapon sets/visibility #
remote func update_local_weapon(player_id, new_index):
	if !(str(player_id) == local_player.net_id):
		get_node("../Space/Players/" + str(player_id)).switch_weapon(new_index)

remote func update_weapon_choices(player_id, weapon_choices):
	if !(str(player_id) == local_player.net_id):
		get_node("../Space/Players/" + str(player_id)).weapons_pack.set_weapons_by_array(weapon_choices)

# ---------- net object updates #
remote func net_object_set_new_authority(object_net_id, new_player_net_id):
	var local_path = Server.net_object_local_paths[str(object_net_id)]
	var to_set = get_node(str(local_path) + "/NetObject")
	to_set.authority_player_net_id = new_player_net_id

# ---------- game mode updates #
remote func gradual_add_health(amount: int, rate: int):
	local_player.gradual_add_health(amount, rate)

remote func gradual_add_health_interrupt():
	local_player.gradual_add_health_interrupt()

# Control Point #
remote func cp_timers_update(cp_timers):
	ModeManager.cp_timers = cp_timers

remote func cp_indicate_capture(net_id, team):
	var local_path = net_object_local_paths[net_id]
	get_node(local_path).indicate_capture(team)

remote func cp_score_update(new_scores):
	ModeManager.scores = new_scores
	local_player.update_score_UI()
