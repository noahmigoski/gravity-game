# Track, if applicable:
# - gamemode related states
# - pickup/interaction related states
# - transform/rot
# Does not apply to more complex net stuff like players.

extends Node
class_name NetObject

var net_id
var local_object
var local_path

# set from parent object
var needs_position_updates = false
var authority_player_net_id = null

var states = {}

func _ready():
	Server.connect("net_objects_initialize", self, "initialize")

func initialize():
	if Server.connected:
		local_object = get_parent()
		local_path = get_parent().get_path()
		register()

# Try to register with the server as net object. 
# If this object is already net-connected, we should 
# just be refreshed with that info (net_id, current states).
func register():
	print("[NetObject: %s] Registering with Server.." % local_object.name)
	Server.reliable_server_call("NetObjectStates", "register", {"local_path": local_path, "initial_states": states})

func _physics_process(_delta):
	if needs_position_updates and net_id != null and Server.local_player.net_id == str(authority_player_net_id):
		var to_send = {"transform": get_parent().global_transform}
		Server.rpc_unreliable_id(1, "update_states_inanimate", net_id, to_send) 
