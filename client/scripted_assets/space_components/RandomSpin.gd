extends Spatial

var rotation_speed: float
var hover_speed: float
var hover_amplitude: float
var dummy: float

# choose some speeds to go at so there's some variation
func _ready():
  rotation_speed = rand_range(.05, .2)
  hover_speed = rand_range(2, 10)
  hover_amplitude = rand_range(.03, .15)
  dummy = 0

# go go go
func _process(delta: float):
  global_transform = global_transform.rotated(Vector3.UP, delta * rotation_speed)
  global_transform = global_transform.translated(Vector3(0, hover_amplitude * cos(dummy * 1.2), 0))
  dummy += delta


# Planet object get this called all the time, but this script is not for 
# planets that are part of gameplay with a path and such. Here, just 
# ignore calls to these for now to squash errors

func _on_GravityField_body_entered(_body):
  # print("ignored an enter")
  pass

func _on_GravityField_body_exited(_body):
  # print("ignored an exit")
  pass

func _on_Surface_body_entered(_body):
  # print("ignored surface body enter")
  pass

func _on_Surface_body_exited(_body):
  # print("ignored surface body exit")
  pass
