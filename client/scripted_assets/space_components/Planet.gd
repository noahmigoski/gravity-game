extends Spatial
class_name Planet # this lets us extend from this script in other scripts.. (i.e., Moon)

onready var planets = $"/root/Space".planets
var planet_index
var mass = 750
var influenced_bodies = []
var surface_bodies = []
var turrets = []
var omega = .000086
onready var R = $Body/CollisionShape.scale.x # Planet radius

# Called when the node enters the scene tree for the first time.
func _ready():
	$Body.set_constant_angular_velocity(Vector3(0,omega,0))
	planet_index = len(planets)
	planets.append(self)
	planet_index += 1 # for next guy

func _process(delta):
	rotate(transform.basis.y.normalized(), omega)
	for body in surface_bodies:
		var surface_vel = (omega * transform.basis.y).cross(R * body.up) * 60 #60 is a fudge factor, not sure why its needed
		if body is KinematicBody:
			body.rotate(body.transform.basis.y.normalized(), omega)
			body.move_and_collide(surface_vel * delta)
		elif body is RigidBody:
			var surface_friction = -(body.get_linear_velocity() - surface_vel)
			body.add_central_force(surface_friction * 60 * body.mu)
			# Use add_force to apply to contact point
			
func _on_GravityField_body_entered(body):
	if (body is KinematicBody || body is RigidBody) && !(self in body.current_g_field):
		body.current_g_field.append(self)
		influenced_bodies.append(body)

func _on_GravityField_body_exited(body):
	if body is KinematicBody:
		if self in body.current_g_field:
			body.current_g_field.erase(self)
		if body in influenced_bodies:
			influenced_bodies.erase(body)

func _on_Surface_body_entered(body):
	if (body is KinematicBody) or (body is RigidBody and !(body is Projectile)):
		surface_bodies.append(body)

func _on_Surface_body_exited(body):
	surface_bodies.erase(body)
