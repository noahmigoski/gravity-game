# The root of any gameplay map
extends Spatial

onready var main_player = $Player

# vars that are passed to individual maps
var flat_gravity = true 

# loading map and play data for this run
signal level_ready
signal send_player_data
var loaded_map_data = {}
var loaded_preplay_data = {}

# all maps keep track of these
const G = 8
var planets = []
var bodies = []
var spawn_points = []
var path_speeds = {}

func _ready():
	# establish self as current scene and connect signals for loading data and exiting
	SceneManager.current_scene = self
	connect("level_ready", SceneManager, "finish_level_setup")
	connect("send_player_data", main_player, "_player_load_data")
	if SceneManager.loaded_by_manager:
		emit_signal("level_ready")
	main_player.connect("player_exit_map", self, "_on_Player_player_exit_map")

func load_map_data(map_data: Dictionary):
	loaded_map_data = map_data
	flat_gravity = loaded_map_data.flat_gravity

func load_preplay_data(play_data: Dictionary):
	loaded_preplay_data = play_data
	emit_signal("send_player_data", loaded_preplay_data) 	# send to player for local load

# TODO: initiate a nice clean up here or in scenemanager 
# so we don't get gameplay errors from returning to menu 
func _on_Player_player_exit_map():
	SceneManager.goto_mainmenu()

# Processing gravity for applicable bodies
# TODO: Try some basic profiling and see about rewriting in GDNative C++
func _process(delta):
	for body in bodies:
		var g = gravity(body)
		body.up = -g.normalized()
		if body is KinematicBody:
			body.vel += delta * g/body.mass
		if body is RigidBody:
			body.add_central_force(g)
	move_along_paths(delta)

func move_along_paths(delta):
	for path in path_speeds:
		path.offset += delta * path_speeds[path]

func gravity(body):
	var r
	var end_gravity = Vector3()
	if body is Player or body is Projectile:
		if !flat_gravity:
			for field in body.current_g_field:
				r = body.global_transform.origin - field.global_transform.origin
				end_gravity += - G * field.mass * body.mass / r.dot(r) * r.normalized()
		else:
			r = Vector3(0, 1, 0) 
			end_gravity += - G * 3 * body.mass / r.dot(r) * r.normalized()
	return end_gravity
