extends Spatial

const SPAWN_TIME = 5
var spawn_timer = 0
var bot = preload("res://scripted_assets/world/enemies/Bot.tscn")

func _process(delta):
	if spawn_timer < SPAWN_TIME:
		spawn_timer += delta
	else:
		var bot_clone = bot.instance()
		bot_clone.global_transform = global_transform
		get_tree().root.add_child(bot_clone)
		spawn_timer = 0
