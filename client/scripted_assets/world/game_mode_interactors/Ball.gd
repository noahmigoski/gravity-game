extends Projectile
class_name Ball

func _ready():
	set_mass(10)
	bodies.append(self)
	friction = mu
	will_self_destruct = false

	# net
	$NetObject.needs_position_updates = true
	$NetObject.states = {
		"object_type": "ball",
		"needs_position_updates": true,
	}
