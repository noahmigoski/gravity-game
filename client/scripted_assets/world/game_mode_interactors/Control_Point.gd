extends Spatial

var has_turrets = false
onready var beacon = $Beacon
onready var light = $Beacon/Light
var beaconMat

var captured_by = ""

var local_id

func _ready():
	local_id = get_instance_id()  
	# try to keep materials unique!
	# TODO: could just have different materials premade and indexed,
	# Then just set to those instead of modifying a unique material.
	var mat = beacon.get_surface_material(0)
	beaconMat = mat.duplicate()
	beacon.set_surface_material(0, beaconMat)

	# Use net_ids in arrays here
	$NetObject.states = {
		"object_type": "control_point",
	}

func _on_Area_body_entered(body):
	if body is Player:
		if Server.connected:
			ModeManager.cp_player_entered_point($NetObject.net_id)
			if body.team != captured_by:
				body.UI.progress_on = true
				body.UI.progress_bar.visible = true
				body.UI.progress_status.text = "Capturing point.." 
			else:
				body.UI.progress_status.text = "Point Secure"

func _on_Area_body_exited(body):
	if body is Player:
		if Server.connected:
			ModeManager.cp_player_exited_point($NetObject.net_id)
			body.UI.progress_on = false 
			body.UI.progress_bar.visible = false
			body.UI.progress_status.text = "" 

func indicate_capture(team):
	captured_by = team
	var color = ModeManager.team_info[team].color
	beaconMat.albedo_color = color
	beaconMat.emission = color
