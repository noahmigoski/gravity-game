extends StaticBody

const grow_rate = .1
var grow_timer = .1
const grow_time = 10
const init_scale = 4

const stage_a = preload("res://assets/models/nature/Models/OBJ format/crops_leafsStageA.obj")
const stage_b = preload("res://assets/models/nature/Models/OBJ format/crops_leafsStageB.obj")
const done = preload("res://assets/models/nature/Models/OBJ format/crop_turnip.obj")

# Called when the node enters the scene tree for the first time.
func _ready():
	$Crop.scale = Vector3(.1,.1,.1) * init_scale


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	#print($RayCast.is_colliding())
	# TODO: dynamically make Raycast point at sun and stretch to fill distance
	# TODO: slow growth rate of plant based on distance to sun
	grow_timer += delta
	if $Crop.scale.x < .99 * init_scale and (not $RayCast.is_colliding()):
		$Crop.scale = Vector3(1,1,1) * grow_timer/grow_time
	if $Crop.scale.x < .66 * init_scale:
		$Crop.mesh = stage_a
	elif $Crop.scale.x > .66 * init_scale and $Crop.scale.x < .99 * init_scale:
		$Crop.mesh = stage_b
	else:
		$Crop.mesh = done

func _on_Pickup_body_entered(_body):
	$Crop.scale = Vector3(.1,.1,.1)
	grow_timer = .1
	$Crop.scale = Vector3(.1,.1,.1) * init_scale
	if "strength" in _body:
		_body.weapons_pack.current_weapon.get_parent().replenish_ammo()
