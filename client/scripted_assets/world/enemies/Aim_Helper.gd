extends Spatial


# Declare member variables here. Examples:
# var a = 2
var targets = []
var fire_time
var fire_timer = 0
const scenes = ["res://local/player/weapons/Dart_Gun/Dart.tscn", "res://local/player/weapons/Grenade_Launcher/Grenade.tscn", "res://local/player/weapons/Rocket_Launcher/Rocket.tscn"]
const fire_rates = {"res://local/player/weapons/Dart_Gun/Dart.tscn": 1, "res://local/player/weapons/Grenade_Launcher/Grenade.tscn":2, "res://local/player/weapons/Rocket_Launcher/Rocket.tscn":3}
const aim_adjust = {"res://local/player/weapons/Dart_Gun/Dart.tscn": 2, "res://local/player/weapons/Grenade_Launcher/Grenade.tscn":2, "res://local/player/weapons/Rocket_Launcher/Rocket.tscn":0}

var projectile_clone
var projectile_scene
var friendly_team = "Red"
var adjust

func _ready():
	randomize()
	var random_scene = scenes[randi() % scenes.size()]
	projectile_scene = load(random_scene)
	fire_time = fire_rates[random_scene]
	adjust = aim_adjust[random_scene]

func _process(delta):
	if targets != []:
		look_at(targets[0].global_transform.origin + targets[0].up * adjust, Vector3(0,0,1))
		if fire_timer >= fire_time:
			fire()
			fire_timer = 0
		else:
			fire_timer += delta

func fire():
	projectile_clone = projectile_scene.instance()
	get_tree().root.add_child(projectile_clone)
	projectile_clone.global_transform = global_transform
	projectile_clone.set_linear_velocity(projectile_clone.global_transform.basis.z * projectile_clone.init_vel * -1)

func _on_Area_body_entered(body):
	if body is KinematicBody && !(body is Projectile):
		if "team" in body:
			if body.team != friendly_team:
				targets.append(body)


func _on_Area_body_exited(body):
	if body is KinematicBody and !(body is Projectile):
		targets.erase(body)
