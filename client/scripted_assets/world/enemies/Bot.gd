extends KinematicBody


onready var current_g_field = []
onready var bodies = $"/root/Space".bodies
const mass = 100
onready var up = Vector3(0,1,0)
onready var vel = Vector3(0,0,0)
var following = []
const SPEED = 4.0
const DAMAGE = 50
var health = 50
var hvel
var random_dir

# Called when the node enters the scene tree for the first time.
func _ready():
	bodies.append(self)
	randomize()
	random_dir = Vector3(rand_range(-1,1), rand_range(-1,1), rand_range(-1,1))

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	if !$AnimationPlayer.is_playing():
		$AnimationPlayer.play("Chop")
		
	if health <= 0:
		bodies.erase(self)
		queue_free()

	vel = move_and_slide(vel, up)
	if following != []:
		look_at(following[0].global_transform.origin, up)
	elif following == []:
		var random_hdir = random_dir.cross(up)
		look_at(transform.origin + random_hdir, up)

	if is_on_floor():
		hvel = global_transform.basis.z * -SPEED
		vel = hvel + vel.dot(up) * up
	
func _on_sword_damage_body_entered(body):
	if "health" in body and body != self:
		body.health -= DAMAGE

func _on_Sight_body_entered(body):
	if body is Player:
		following.append(body)


func _on_Sight_body_exited(body):
	if body in following:
		vel = Vector3(0,0,0)
		following.erase(body)
		randomize()
		random_dir = Vector3(rand_range(-1,1), rand_range(-1,1), rand_range(-1,1))

