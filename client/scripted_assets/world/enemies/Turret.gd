extends Spatial


onready var home_planet = get_parent()
func _ready():
	if home_planet.get("turrets"):
		home_planet.turrets.append(self)

func set_team(team):
	$Base.get_surface_material(0).albedo_color = team["color"]
	$Base.get_surface_material(0).emission = team["color"]
	$Aim_Helper.friendly_team = team
