extends Spatial
const ammo_amount = 60
const RESPAWN_TIME = 5
var respawn_timer = RESPAWN_TIME

var is_ready

func _ready():
	is_ready = false

func _physics_process(delta):
	visible = is_ready
	if !is_ready:
		if respawn_timer > 0:
			respawn_timer -= delta
		if respawn_timer <= 0:
				is_ready = true
				respawn_timer = RESPAWN_TIME

func _on_Area_body_entered(body):
	if "strength" in body && is_ready:
		body.weapons_pack.current_weapon.get_parent().replenish_ammo() # awkward but functional
		is_ready = false
