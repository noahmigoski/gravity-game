extends Area
class_name Interactable

onready var player = get_node("/root/Space/Player")
signal interactable_touch
var prompt_to_player;

func _ready():
	prompt_to_player = "Press {bluh} to interact" # default
	connect("interactable_touch", player, "handle_interactable")

# OVERRIDE this to define effect of interacting
func enable_interaction():
	pass

# ------------ Signals to player to change ui -------- #
func _on_Area_body_entered(body):
	if body is Player:
		emit_signal("interactable_touch", prompt_to_player, self)

func _on_Area_body_exited(body):
	if body is Player:
		emit_signal("interactable_touch", "", self)
