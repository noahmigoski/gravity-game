### Server 

A dedicated server setup in Godot requires (or is at least best done by) creating a separate project/executable that runs on the server system which client executables connect to and communicate with. 

This means game assets of any kind definitely go in client/, while the sharing of information between players and various game mode details would happen in this server project. Make a point to focus on getting the functionality right before performance; it's probably not worth worrying about until way later in the process.

In the end, a *lot* of the functionality that is easily coded in the client-side will end up having to be included in the server-side to communcate that to other clients. Client-side functionality allows a user to feel that at least *their* player responds smoothly to any input and looks accurate and as polished as possible. The server is where we communicate the details necessary to make other players able to make decisions and interact with outside objects. 

#### Usage

Ensure that ip/port numbers agree between the main Server.gd scripts so that they connect to each other correctly. If you are going to run the server/client *in their own editors*, not as executables, make sure that the port numbers under Editor Settings > Network > Debug are available and different between the editors so that they have their own functioning debug sections. 

Otherwise you can export the server as an executable, run that, then run the client in-editor without making this change (debug output appears in a terminal somewhere). 
