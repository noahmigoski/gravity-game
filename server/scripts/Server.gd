extends Node

# This is the GAME server; this is what would run when a lobby is decided 
# and people are about to actually game. Eventually we would need some 
# AuthServer setup so that people can browse servers and do network things 
# outside of the actual gaming.

# boilerplate from https://www.youtube.com/watch?v=lnFN6YabFKg&t=343s

var network = NetworkedMultiplayerENet.new()
var port = 4567 
var max_players = 50

var player_temp = load("res://scripts/PlayerTemplate.tscn")
onready var player_states = $"../Space/PlayerStates" 
onready var players = $"../Space/Players"
onready var net_object_states = $"../Space/NetObjectStates"
onready var debug_gui = $"../Space/InfoDebug/MarginContainer/VBoxContainer"

# ------------ Starting the server, user connections ------ #

func _ready():
	StartServer()
	
func StartServer():
	network.create_server(port, max_players)
	get_tree().set_network_peer(network)
	print("Server started")
	
	network.connect("peer_connected", self, "_Peer_Connected")
	network.connect("peer_disconnected", self, "_Peer_Disconnected")
	
func _Peer_Connected(player_id):
	# store a template of this player on server
	player_states.all_players.append(player_id)
	var new_player_instance = player_temp.instance()
	new_player_instance.name = str(player_id)
	players.add_child(new_player_instance)

	# send client their net_id, inform other clients,
	# update this client on current states/other players 
	rpc_id(player_id, "set_my_net_id", str(player_id))
	if (len(player_states.all_players) > 0):
		for p in player_states.all_players:
			rpc_id(player_id, "add_connected_player", str(p))
	rpc_id(0, "add_connected_player", str(player_id))


	
func _Peer_Disconnected(player_id):
	# free and replace authority for net objects if needed
	player_states.all_players.erase(player_id)
	if len(player_states.all_players) == 0:
		for o in get_node("../Space/Players/" + str(player_id)).authority_over:
			get_node("../Space/NetObjects/" + str(o)).authority = null
	else:
		for o in get_node("../Space/Players/" + str(player_id)).authority_over:
			var orphaned_object = get_node("../Space/NetObjects/" + str(o))
			orphaned_object.authority = player_states.all_players[0]
			Server.rpc_id(0, "net_object_set_new_authority", o, orphaned_object.authority)

	# remove from server records
	player_states.all_player_states.erase(str(player_id))
	get_node("../Space/Players/" + str(player_id)).queue_free()

	# tell clients to remove this player 
	rpc_id(0, "remove_disconnected_player", player_id)

	debug_gui.set_player_text()
# ------------- UDP updates ------------ #
	
# continuous authoritative updates to peers
func _physics_process(_delta):
	rpc_unreliable_id(0, "update_local_transforms", player_states.all_player_states)

remote func update_states(new_states):
	var sender = get_tree().get_rpc_sender_id()
	players.get_node(str(sender)).update_player_states(new_states)

remote func update_states_inanimate(net_id, new_states):
	var to_update = get_node("/root/Space/NetObjects/" + net_id)
	to_update.cur_transform = new_states["transform"]
	rpc_unreliable_id(0, "net_object_loc_update", net_id, new_states)

# ------------- TCP updates ------------ #

# Funnel calls to other nodes with any info they may need to keep this file clean
# @var funnel_category: PlayerStates, Test, etc.: string of any scripted node here
# @var function: the function within the funnel_category to call
# @var data[0..] = any data needed to perform that function
remote func reliable_server_call(funnel_category: String, function: String, data = {}):
	var sender = get_tree().get_rpc_sender_id()
	if funnel_category == "Player":
		players.get_node(str(sender)).call(function, sender, data)
	elif funnel_category == "ModeManager":
		ModeManager.call(function, sender, data)
	else:
		get_node("../Space/" + funnel_category).call(function, sender, data)
