extends Node

func fire_projectile(sender, _data):
	Server.rpc_id(0, "update_fire_projectile", sender)

func stop_continuous_fire(sender, _data):
	Server.rpc_id(0, "stop_continuous_fire", sender)
