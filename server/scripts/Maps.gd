extends Node

# Store specific info about maps for spawn locations, etc.
var spawns_TEST = [Vector3(-8, 42, -25), Vector3(-8, -14, 36)]

func get_spawn(map_name):
	# funnel to correct spawn options
	if (map_name == "test"):
		var rand_index = rand_range(0, len(spawns_TEST))
		return spawns_TEST[rand_index]
