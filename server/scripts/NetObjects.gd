extends Node

# Infrastructure to register and update net objects besides players.

onready var NetObjectsContainer = get_node("/root/Space/NetObjects")
var object_template = load("res://scripts/NetObjectTemplate.tscn")

# Translate local path to net id
var path_to_id = {}

# Save ids for specific sets of objects for easy access out of all net objects
var all_net_objects = []
var net_objects_need_authority = []
var control_point_ids = []

func register(sender, data):
	# we have not registered this before
	if !(data.local_path in path_to_id):
		# Setup local template
		var new_template = object_template.instance()
		new_template.name = str(new_template.get_instance_id())
		NetObjectsContainer.add_child(new_template)
		path_to_id[data.local_path] = new_template.name
		all_net_objects.append(new_template.name)

		# Save ids by object type, set default values if needed
		if "object_type" in data.initial_states:
			if data.initial_states["object_type"] == "control_point":
				control_point_ids.push_back(new_template.name)
				new_template.states = {
					"object_type": "capture_point",
					"is_captured": false,
					"captured_by": "",
					"num_players_on": 0,
					"players_on": [],
					"capture_rates": {},
					"capture_timers": {},
					"has_turrets": false,
					"turrets": [],
				}
			elif data.initial_states["object_type"] == "ball":
				new_template.needs_authority = true
				net_objects_need_authority.append(new_template.name)

		# Confirm to client
		Server.rpc_id(sender, "register_net_object", new_template.name, data.local_path, new_template.states)
		# If needs authority, make sure it has one
		if "needs_position_updates" in data.initial_states and new_template.authority == null:
			new_template.authority = sender
			Server.players.get_node(str(sender)).authority_over.append(new_template.name)
			Server.rpc_id(0, "net_object_set_new_authority", new_template.name, sender)
	# It has been registered, just send that data
	else:
		var registered_net_id = path_to_id[data.local_path]
		var existing_node = NetObjectsContainer.get_node(registered_net_id) 
		if existing_node.cur_transform != null and existing_node.authority != null:
			existing_node.states["cur_transform"] = existing_node.cur_transform
			existing_node.states["authority"] = existing_node.authority
		Server.rpc_id(sender, "register_net_object", registered_net_id, data.local_path, existing_node.states)
