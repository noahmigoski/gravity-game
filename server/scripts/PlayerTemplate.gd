extends Node

# ----------- UDP updates/states ---------- #

onready var player_states = get_node("../../PlayerStates")
var states = {}
var authority_over = []

func update_player_states(new_states):
	states = new_states
	player_states.all_player_states[name] = new_states

# ----------- TCP ---------- #

var current_weapon_index: int
var current_weapon_set: Array 
var team: String
var max_health: int

func update_cur_weapon(sender, data):
	current_weapon_index = data.new_index
	Server.rpc_id(0, "update_local_weapon", sender, current_weapon_index)

func load_weapon_choices(sender, data):
	current_weapon_set = data.weapon_choices
	Server.rpc_id(0, "update_weapon_choices", sender, data.weapon_choices)
	# FIXME: team not always decided by player
	var player_to_setup = Server.players.get_node(str(sender))
	player_to_setup.team = data.team
	player_to_setup.max_health = data.max_health

	Server.debug_gui.set_player_text()

# collect and send catchup data to a new player (right now weapon sets, could be lots of things)
func catchup_data(sender, _data):
	var all_player_objects = get_node("/root/Space/Players").get_children()
	var catchup_data_store = {}
	for p in all_player_objects:
		var temp_data = {} # fill with needed data categories
		temp_data["weapon_choices"] = p.current_weapon_set
		temp_data["team"] = p.team
		catchup_data_store[str(p.name)] = temp_data # add that map to full data map per online player
	Server.rpc_id(sender, "recieve_catchup_data", catchup_data_store)

	# if there are net objects without authority that need it,
	# give it to this player
	var need_authority = Server.net_object_states.net_objects_need_authority
	for need_id in need_authority:
		var need = get_node("/root/Space/NetObjects/" + need_id)
		if need.authority == null:
			need.authority = sender 
			Server.rpc_id(0, "net_object_set_new_authority", need_id, sender)
