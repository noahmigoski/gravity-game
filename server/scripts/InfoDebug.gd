extends VBoxContainer

# Much more convenient way to debug server when not in cmd only

onready var mode = $mode
onready var scores = $scores
onready var other_entries = $other_entries
onready var players_entries = $players_entries

var should_append_other_text = false

func _ready():
	mode.text = "Gamemode Index: " + str(ModeManager.current_mode) + "\n"
	set_score_text()


func gen_spacing(spacing: int):
	var i = spacing
	var end = ""
	while i > 0:
		end += " "
		i -= 1	
	return end

# Generate a Vbox of rows for UI given a possibly-nested dictionary
func print_dict_to_rows(dict: Dictionary, vbox=null):
	var all_rows
	if vbox != null:
		all_rows = vbox
	else:
		all_rows = VBoxContainer.new()

	var local_vbox = VBoxContainer.new()
	for e in dict.keys():
		# add new row to local vbox for current dict
		var new_row = make_label_row([e, dict[e]])
		local_vbox.add_child(new_row)
	
	# add new set to full set and return it
	all_rows.add_child(local_vbox)
	return all_rows 

func print_array_to_rows(arr: Array):
	var vbox = VBoxContainer.new()
	for e in arr:
		var insert_text = "  " + str(e)
		var new_row = make_label_row([insert_text])
		vbox.add_child(new_row)
	return vbox

# Supply an array of entries to print on one row (in an hbox)
func make_label_row(elements: Array):
	var hbox = HBoxContainer.new()
	for e in elements:
		var new_label = Label.new()
		new_label.text = str(e)
		new_label.align = Label.ALIGN_CENTER
		new_label.size_flags_horizontal = Label.SIZE_EXPAND
		hbox.add_child(new_label)
	return hbox

func set_score_text():
	for n in scores.get_children():
		scores.remove_child(n)
		n.queue_free()
	var title1 = make_label_row(["Team", "Score"])
	var title2 = make_label_row(["------", "------"])
	scores.add_child(title1)
	scores.add_child(title2)
	var new_scores_set = print_dict_to_rows(ModeManager.scores)
	scores.add_child(new_scores_set)

func set_player_text():
	for n in players_entries.get_children():
		players_entries.remove_child(n)
		n.queue_free()
	for id in Server.player_states.all_players:
		var player_team = get_node("/root/Space/Players/" + str(id)).team
		var new_label = make_label_row([id, player_team])
		players_entries.add_child(new_label)

# Supply info.title and info.data
func set_other_text(info: Dictionary):
	for n in other_entries.get_children():
		other_entries.remove_child(n)
		n.queue_free()
	var title = make_label_row([info.title])
	if info.data is Dictionary:
		var data_ui = print_dict_to_rows(info.data)
		other_entries.add_child(title)
		other_entries.add_child(data_ui)
	elif info.data is Array:
		var data_ui = print_array_to_rows(info.data)
		other_entries.add_child(title)
		other_entries.add_child(data_ui)

func _physics_process(_delta):
	# only do these updates if debug is visible and we want to see it
	if visible:
		pass
