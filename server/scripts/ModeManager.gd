extends Node 

# ----------- applicable to all modes ------------ #
onready var NetObjectStates = get_node("/root/Space/NetObjectStates")
onready var NetObjectsContainer = get_node("/root/Space/NetObjects")

var active_teams = ["Red", "Blue", "Green"]
var team_info = {
	"Red": {
		"color": "#ff0000",
		"players": []
	},
	"Green": {
		"color": "#00ff00",
		"players": []
	},
	"Blue": {
		"color": "#0000ff",
		"players": []
	},
} # team_name -> {color, players, ...}

var MAX_PLAYERS_TO_TEAM
var GAME_TIME_LIMIT 
var game_timer 
var scores # team_name -> score (int or whatever applies)
enum game_modes {
	NO_GAMEPLAY,
	CP,
	TDM,
	SND,
	GOLF,
}
var current_mode

# TODO: Who decides what game mode this server runs and when
# (has to be option from player for player-hosted and somehow dedicated decisions)
func _ready():
	current_mode = game_modes.NO_GAMEPLAY 
	ModeManager.cp_reset()

# ----------- GAME MODE RESET / SETUPS ------ #
# Net objects should already be initialized at this point

func gamemode_catchup(sender, _data):
	if current_mode == game_modes.CP:
		cp_catchup(sender)


# CONTROL POINT #

const CP_CAPTURE_TIME = 2.0
const CP_SCORE_INCREMENT_TIME = 3.0
const CP_SCORE_INCREMENT_AMOUNT = 1
const CP_WINNNING_SCORE = 100

var cp_timers = {}
var score_increment_timer = CP_SCORE_INCREMENT_TIME

func cp_reset():
	current_mode = game_modes.CP
	scores = {}
	for t in active_teams:
		scores[t] = 0


func cp_catchup(player_id):
	# fill current point data to catchup with
	var cp_states = {}
	for p_id in NetObjectStates.control_point_ids:
		var p = NetObjectsContainer.get_node(p_id)
		cp_states[p_id] = p.states
	# send package of catchup data
	var to_send = {}
	to_send.mode = current_mode
	to_send["scores"] = scores
	to_send["cp_states"] = cp_states
	Server.rpc_id(player_id, "gamemode_catchup", to_send)
	

func cp_player_entered_point(sender, data):
	var entered_point_states= NetObjectsContainer.get_node(data.point_id).states
	var player_object = Server.players.get_node(str(sender)) 

	# Register player as on this point
	var team_name = player_object.team
	entered_point_states.num_players_on += 1
	entered_point_states.players_on.push_back(player_object)
	# If I'm the first capturer of my team, get it going
	if not team_name in entered_point_states.capture_rates:
		entered_point_states.capture_timers[team_name] = CP_CAPTURE_TIME
		entered_point_states.capture_rates[team_name] = 1
	else:
		entered_point_states.capture_rates[team_name] += 1

	# If it's already captured for me, heal up
	if entered_point_states.is_captured and entered_point_states.captured_by == team_name:
		Server.rpc_id(sender, "gradual_add_health", (player_object.max_health - player_object.states.health), 6)

func cp_player_exited_point(sender, data):
	var exited_point_states = NetObjectsContainer.get_node(data.point_id).states
	var player_object = Server.players.get_node(str(sender)) 

	exited_point_states.num_players_on -= 1
	exited_point_states.capture_rates[player_object.team] = clamp(exited_point_states.capture_rates[player_object.team] - 1, 0, 5) 
	exited_point_states.players_on.erase(player_object)
	Server.rpc_id(sender, "gradual_add_health_interrupt")

	# Reset (or run down) everybody's progress to capturing if no one is on and it is not captured for them
	if exited_point_states.num_players_on == 0:
		for r in exited_point_states.capture_rates:
			if !exited_point_states.is_captured or (exited_point_states.captured_by != player_object.team) and exited_point_states.capture_timers[player_object.team] != 0:
				exited_point_states.capture_rates[r] = -1 

func cp_score_increment():
	for point_id in NetObjectStates.control_point_ids:
		var point = NetObjectsContainer.get_node(point_id).states
		if point.is_captured:
			scores[point.captured_by] += CP_SCORE_INCREMENT_AMOUNT

func cp_timer_rundowns(delta):
	# DEBUG
	var debug_states = []

	# Run timers for being on points
	for point_id in NetObjectStates.control_point_ids:
		var point = NetObjectsContainer.get_node(point_id).states

		# DEBUG
		debug_states.append(point)

		if point.num_players_on == 0:
			for team in point.capture_timers.keys():
				# Running timer back up when no one is on point
				if point.capture_timers[team] < CP_CAPTURE_TIME and point.capture_rates[team] < 0:
					point.capture_timers[team] -= delta * point.capture_rates[team]
				# Reset rate to 0 if they are run back up already
				if point.capture_rates[team] < 0 and point.capture_timers[team] >= CP_CAPTURE_TIME:
					point.capture_rates[team] = 0
		elif point.num_players_on > 0:
			for team in point.capture_timers.keys():
				# Someone is capturing but not succeeded yet
				# TODO: AND NOT CONTESTED
				if point.capture_timers[team] > 0:
					point.capture_timers[team] -= delta * point.capture_rates[team]
				# The point is captured for this team
				elif point.capture_timers[team] <= 0:
					point.is_captured = true
					point.captured_by = team
					point.capture_rates[team] = 0
					Server.rpc_id(0, "cp_indicate_capture", point_id,  team)
					for p in point.players_on:
						if p.team == point.captured_by:
							Server.rpc_id(int(p.name), "gradual_add_health", (p.max_health - p.states.health), 3)
					if point.has_turrets:
						for t in point.turrets:
							t.set_team(team)

		# update just timers to comm to clients
		cp_timers[point_id] = point.capture_timers
	
	# DEBUG
	Server.debug_gui.set_other_text({"title": "Point States", "data": debug_states})
	
	# Run timers for maintaining overall score
	if score_increment_timer > 0.0:
		score_increment_timer -= delta 
	else:
		cp_score_increment()
		score_increment_timer = CP_SCORE_INCREMENT_TIME
		Server.rpc_id(0, "cp_score_update", scores)
		# DEBUG
		Server.debug_gui.set_score_text()
# OTHER MODE

# Run down timers
func _physics_process(delta):
	if current_mode == game_modes.CP:
		cp_timer_rundowns(delta)
		Server.rpc_id(0, "cp_timers_update", cp_timers)
		

func _process(_delta):
	pass
