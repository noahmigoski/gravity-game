# Gravity Game
First-person shooter with planet-based gravity. Players launch themselves between planets with rocket-jumping, and weapons fire physics-aware projectiles. Also, explosions.

Developed with networked multiplayer in mind for main PC targets.

### Links
> NOTE: Web Demo not a primary target platform and is not updated consistently. Press space bar to capture mouse after loading.

- [Gravity Game Web Demo](https://migoski.net/apps/web_grav_game/web_grav_game.html)
- [Trello Issue Tracking](https://trello.com/b/HPuOLrNV/gravitygame)
